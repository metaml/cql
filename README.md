CQL Binary Protocol Implementation
==================================

This Haskell library implements Cassandra's CQL Binary Protocol versions
[3] and [4]. It provides encoding and decoding functionality as well
as representations of the various protocol related types.

Contributing
------------

If you want to contribute to this project please read the file
CONTRIBUTING first.

[3]: https://github.com/apache/cassandra/blob/trunk/doc/native_protocol_v3.spec
[4]: https://github.com/apache/cassandra/blob/trunk/doc/native_protocol_v4.spec
